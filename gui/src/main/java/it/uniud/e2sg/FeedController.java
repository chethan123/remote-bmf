package it.uniud.e2sg;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/websocket/feed",
        encoders = {FeedEncoder.class},
        decoders = {FeedDecoder.class})
public class FeedController {

    private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());
    
    private static final Random rnd = new Random();
    private static Generator generator;
    
    private static class Generator extends Thread {
    
    	private volatile boolean stopped = false;
    	
    	@Override
    	public void run() { 
 
    		while (!stopped) {
    			try {
	    			double val = rnd.nextDouble();
	    			
	    			JsonObject jsonValue = Json.createObjectBuilder()
	    				     .add("value", val)
	    				     .build();
	    			
	    			Feed feed = new Feed(jsonValue);
	    			
	    	        System.out.println("Broadcast value: " + val);
	    	        for (Session peer : peers) {
	    	            peer.getBasicRemote().sendObject(feed);
	    	        }
	    			try {
	    				Thread.sleep(1000);
	    			} catch (InterruptedException e) {
	    				
	    			}
    			} catch (IOException|EncodeException e) {
    				
    			}
    		}
    	} 
    	
    	public void stopGeneration() {
    		stopped = true;
    	}
    }

    @OnOpen
    public void onOpen(Session peer) {
        peers.add(peer);
    }
    
    @OnClose
    public void onClose(Session peer) {
        peers.remove(peer);
    }

    @OnMessage
    public void switchFeed(String message, Session session) throws IOException {
    	if (generator == null) {
    		generator = new Generator();
    		generator.start();
    		System.out.println("Generation started");    		
    	} else if (!generator.isAlive()) {
    		generator = new Generator();
    		generator.start();
    		System.out.println("Generation resumed");
    	} else {
    		generator.stopGeneration();
    		System.out.println("Generation stopped");
    	}
    }
}
