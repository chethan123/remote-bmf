package it.uniud.e2sg;

import java.io.StringWriter;
import javax.json.Json;
import javax.json.JsonObject;

public class Feed {
	
    private JsonObject json;

    public Feed() { }
    
    @Override
    public String toString() {
        StringWriter writer = new StringWriter();
        Json.createWriter(writer).write(json);
        return writer.toString();
    }

    public Feed(JsonObject json) {
        this.json = json;
    }

    public JsonObject getJson() {
        return json;
    }

    public void setJson(JsonObject json) {
        this.json = json;
    }
    
}
