package it.uniud.e2sg.examples.counter;

public class Counter {

    private static int count; 
    
    public synchronized void doIncrease() {
        count++;
    }

    public synchronized int getCount() {
        return count;
    }
}