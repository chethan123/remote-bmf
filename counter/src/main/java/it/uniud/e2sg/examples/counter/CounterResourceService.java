package it.uniud.e2sg.examples.counter;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import javax.ws.rs.Path;

@Path(CounterResource.PATH)
@ServerEndpoint(value = "/counter")
public final class CounterResourceService implements CounterResource {
    
	private static Counter counter = null;
    private static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

    @OnOpen
    public void onOpen(Session peer) {
        peers.add(peer);
    }
    
    @OnClose
    public void onClose(Session peer) {
        peers.remove(peer);
    }
	
    public String getCount() {
        
    	if (counter == null)
    		counter = new Counter();
        return Integer.toString(counter.getCount());
    }
    
	public synchronized void increaseCount() {
		if (counter != null)
			counter.doIncrease();
	}
	
    @OnMessage
    public void broadcastNewCount(ByteBuffer data, Session session) throws IOException {
    	
    	increaseCount();
    	
    	String countTxt = getCount();
    	
        for (Session peer : peers)
            peer.getBasicRemote().sendText(countTxt);
    }
}


