package it.uniud.e2sg.examples.counter;

import javax.ws.rs.core.*;
import javax.ws.rs.*;

public interface CounterResource {
    
	public static String PATH = "counter";
	
	@GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getCount();
	
	@POST
	public void increaseCount();
}
