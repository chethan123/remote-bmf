var wsUri = "ws://" + document.location.host + document.location.pathname + "counter";
var websocket = new WebSocket(wsUri);
websocket.binaryType = "arraybuffer";

websocket.onmessage = function(evt) { onMessage(evt) };

function sendIncrease() {
    websocket.send(new ArrayBuffer(1));
}
                
function onMessage(evt) {
    console.log("received: " + evt.data);
    document.getElementById("textualcount").innerHTML = evt.data;
}
